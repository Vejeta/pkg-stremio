Document: stremio
Title: Debian stremio Manual
Author: <insert document author here>
Abstract: This manual describes what stremio is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/stremio/stremio.sgml.gz

Format: postscript
Files: /usr/share/doc/stremio/stremio.ps.gz

Format: text
Files: /usr/share/doc/stremio/stremio.text.gz

Format: HTML
Index: /usr/share/doc/stremio/html/index.html
Files: /usr/share/doc/stremio/html/*.html
